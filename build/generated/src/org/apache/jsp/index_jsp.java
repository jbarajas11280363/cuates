package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");

String error=request.getParameter("error");
if(error==null || error=="null"){
 error="";
}

      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Login JSP</title>\n");
      out.write("<script>\n");
      out.write("    function trim(s) \n");
      out.write("    {\n");
      out.write("        return s.replace( /^s*/, \"\" ).replace( /s*$/, \"\" );\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    \n");
      out.write("     function validarNum(evt)\n");
      out.write("{\n");
      out.write("var charCode = (evt.which) ? evt.which : event.keyCode\n");
      out.write("if (charCode > 31 && (charCode < 48 || charCode > 57))\n");
      out.write("return false;\n");
      out.write(" \n");
      out.write("return true;\n");
      out.write("}\n");
      out.write("</script>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("<div>");
      out.print(error);
      out.write("</div>\n");
      out.write("<form name=\"frmLogin\"  action=\"login.jsp\" method=\"post\">\n");
      out.write("Usuario <input type=\"text\" name=\"name\" required=\"es necesario\" /><br />\n");
      out.write("Contraseña <input  onkeypress=\"return validarNum(event)\" type=\"password\" name=\"password\" required=\"es necesario\" /><br />\n");
      out.write("<input type=\"submit\" name=\"submit\" value=\"Entrar\" />\n");
      out.write("</form>\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
